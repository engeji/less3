"""Модуль для асинхронных запросов OMW на основе файла cities.txt
"""
import asyncio
import configparser  # импортируем библиотеку
import re
import time
from functools import total_ordering
from typing import List

import aiohttp


config = configparser.ConfigParser()  # создаём объекта парсера
config.read("settings.ini")  # читаем конфиг
toren = config["OWA"]["token"]
@total_ordering
class CityWeather:
    """Класс ГородПогода. Инкопсулирует информацию о погоде в городе
    """
    req:List['CityWeather'] = []

    openweatherAPI = toren

    def __format__(self, __format_spec: str) -> str:
        return f'{self.city_name:{__format_spec}}'

    def __eq__(self, other):
        return self._data['main']['feels_like'] == other._data['main']['feels_like']

    def __lt__(self, other):
        return self._data['main']['feels_like'] < other._data['main']['feels_like']

    @property
    def get_url(self)->str:
        """строка подключения для api OWM
        """
        return f'https://api.openweathermap.org/data/2.5/' \
            f'weather?q={self.city_name}&appid={CityWeather.openweatherAPI}&units=metric'

    def __init__(self,city_name:str) -> None:
        self.city_name = city_name
        self._data:dict = None
        self._timeout = None
        self.req.append(self)

    def __repr__(self) -> str:
        begin_line = f'\nТемпература в {self.city_name}'
        if self._data is None:
            return f'{begin_line} (запрос не был отправлен)'
        t_min = self._data['main']['temp_min']
        t_max = self._data['main']['temp_max']
        t_feel = self._data['main']['feels_like']
        speed = self._data['wind']['speed']
        return f'{begin_line}\n'\
            f'    Температура от {t_min:.0f} до {t_max:.0f} C.\n'\
            f'    по ощущению {t_feel} C.\n' \
            f'    Скороcть ветра {speed} м/с' \
 
    async def async_http_get(self,client:aiohttp.ClientSession):
        start_time = time.time()
        async with client.get(self.get_url) as resp:
            assert resp.status == 200, f'видимо OWM упал с ошибкой {resp.status}'
            data = await resp.json()
            self._timeout = time.time() - start_time
            self._data = data


    @staticmethod
    async def async_main():
        """Статичный метод для создания сессии для формирования курутин
        для функции asyncio.gather для всех запросов по городам в CityWeather.req
        """
        async with aiohttp.ClientSession() as client:
            await asyncio.gather(*[
                    asyncio.ensure_future(item.async_http_get(client))
                for item in CityWeather.req
        ])

    @staticmethod
    def weather_raiting():
        """Статичный метод для асинхронного вызова списка курутин в текущем
        цикле событий asyncio.get_event_loop
        """
        stat_time = time.time()
        loop = asyncio.get_event_loop()
        loop.run_until_complete(CityWeather.async_main())
        loop.close()

        print('Сумма времени всех запросов:' \
              f'{sum((item._timeout for item in CityWeather.req)):.0f} сек')

        city_max_timeout = max(CityWeather.req, key=lambda x: x._timeout)
        print('Cамый длительный запрос города:' \
              f'{city_max_timeout.city_name} в {city_max_timeout._timeout:.2f} сек')

        print(f'Время выполнения: {time.time() - stat_time:.2f} сек')

if __name__ == "__main__":
    with open('cities.txt', encoding="utf-8") as f:
        file_content = f.readlines()

    city_name_list = [''.join(re.findall(r'[a-zA-Z ]', line)).strip() for line in file_content[1:]]

    for city_name in city_name_list:
        CityWeather(city_name)

    CityWeather.weather_raiting()
    print('-'*40)
    print(f"Самый теплый город {max(CityWeather.req)}")
    print('-'*40)
    print(f"Самый холодный город {min(CityWeather.req)}")
    print('-'*40)
    print('Сортировка по температуре')
    print(*sorted(CityWeather.req))
